const BASE_URL = 'https://jsonplaceholder.typicode.com';
export const environment = {
  production: true,
  USERS: BASE_URL + '/users'
};


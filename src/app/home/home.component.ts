import { Component, OnInit } from '@angular/core';
import { HttpConnectorService } from '../shared/http-connector.service';
import { environment } from 'src/environments/environment';
import { DataSharingService } from '../shared/data-sharing.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
  userData: Promise<any>;
  userDetails: any;
  constructor(private httpConnector: HttpConnectorService, private dataShare: DataSharingService) { }

  ngOnInit(): void {
    this.dataShare.getData.subscribe(data => this.userDetails = data);
    this.userData = this.getData();
  }

  async getData(): Promise<any> {
    const response = await this.httpConnector.getRequest(environment.USERS).toPromise();
    return response;
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataSharingService } from 'src/app/shared/data-sharing.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isSubmitted = false;
  userData: any;
  constructor(private formBuilder: FormBuilder, private router: Router, private dataShare: DataSharingService) { }

  ngOnInit(): void {
    this.dataShare.getData.subscribe(data => this.userData = data);
    this.loginForm = this.formBuilder.group({
      email: [this.userData && this.userData.email ? this.userData.email : '', [Validators.required, Validators.email]],
      password: [this.userData && this.userData.password ? this.userData.password : '', [Validators.required]]
    });
  }
  get formControls() { return this.loginForm.controls; }
  login() {
    console.log(this.loginForm.value);
    this.isSubmitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.dataShare.setData(this.userData);
    this.router.navigateByUrl('/home');
  }
}

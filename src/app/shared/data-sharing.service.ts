import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataSharingService {

  private messageSource = new BehaviorSubject({});
  getData = this.messageSource.asObservable();

  constructor() { }

  setData(message: any) {
    this.messageSource.next(message)
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpConnectorService {
  ;
  constructor(private http: HttpClient) { }

  // reusable method to handle all http request method "GET"
  getRequest(url: string): Observable<any> {
    return this.http.get(url).pipe(
      catchError(err => this.throwAlert(err, url))
    );
  }


  throwAlert(err: Response, url: string) {
    const errCode = err.status ? err.status : '';
    return errCode.toString() + err + url;
  }
}

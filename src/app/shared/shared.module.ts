import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpConnectorService } from './http-connector.service';
import { DataSharingService } from './data-sharing.service';
import { SearchPipe } from './filters.pipe';
@NgModule({
  imports: [
    
  ],
  declarations: [SearchPipe],
  exports: [
    SearchPipe
  ],
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [HttpConnectorService, DataSharingService],

    };
  }
}